import json
import time
import os
import sys
import uuid

import zmq
from ccu import CCU
from datetime import datetime


FREQ = 10


def stream_media(audio_media, video_media):
    print('Streaming...')

    video_socket = CCU.socket(CCU.queues['VIDEO_MAIN'], zmq.PUB)
    audio_socket = CCU.socket(CCU.queues['AUDIO_ENV'], zmq.PUB)

    if len(audio_media) != len(video_media):
        print('Length of media queues do not match!')
        quit()

    processing_num = 0
    start_time = time.time()
    mark_start_time = datetime.now()
    audio_id = str(uuid.uuid4())
    sample = 0

    while processing_num < len(video_media):   
        video_msg = video_media[processing_num]
        #video_msg['count'] = processing_num
        video_msg['timestamp'] = .1 * processing_num
        video_msg['datetime'] = str(datetime.now())
        #video_msg['uuid'] = uuid.uuid4()
        video_msg['width'] = 1920
        video_msg['height'] = 1080
        video_msg['depth'] = 3
        video_msg['format'] = 'jpg'
        video_msg['container_name'] = 'parc-stream-publish-video'
        video_msg['trigger_id'] = {}
        CCU.send(video_socket, video_msg)
        
        audio_msg = audio_media[processing_num]
        #audio_msg['count'] = processing_num
        audio_msg['timestamp'] = .1 * processing_num
        audio_msg['datetime'] = str(datetime.now())
        #audio_msg['uuid'] = uuid.uuid4()
        audio_msg['sample_rate'] = 16000
        audio_msg['bit_depth'] = 16
        audio_msg['num_samples'] = len(audio_media)
        audio_msg['channels'] = 1
        audio_msg['start_sample'] = 0
        audio_msg['sample'] = sample
        sample += 1
        mark_time = datetime.now()
        delta = mark_time - mark_start_time
        audio_msg['start_seconds'] = float(delta.total_seconds())
        audio_msg['audio_id'] = audio_id
        audio_msg['speaker'] = 'FLE'
        audio_msg['container_name'] = 'parc-stream-publish-audio'
        audio_msg['trigger_id'] = {}
        CCU.send(audio_socket, audio_msg)

        end_time = time.time()

        val = ((1/FREQ) - 0.0005) - (end_time- start_time)
        if val < 0:
            val = 0
        time.sleep(val)

        #while((end_time- start_time) < (1/FREQ)):
        end_time = time.time()            

        print('Sent messages {} of {} with a time delta of {} and datetime of {}'.format(processing_num, 
            len(video_media), str(end_time- start_time), audio_msg['datetime']))
        processing_num += 1

        start_time = end_time

def load_media(libname, audio_media_names, video_media_names, audio_media, video_media):
    print('Loading {}...'.format(libname))

    for root, dirs, files in os.walk("library/" + libname, topdown=False):
       number = 1
       for name in files:
          if 'jpg' in name:
            video_media_names.append(name)
          elif 'wav' in name:
            audio_media_names.append(name)

    video_media_names.sort()
    audio_media_names.sort()

    #print(video_media_names[:10])
    #print(audio_media_names[:10])

    # Video
    for image_file_name in video_media_names:
        message = CCU.base_message('webcam')
        with open("library/" + libname + '/' + image_file_name, 'rb') as file:
            image = file.read()
            message['image'] = CCU.binary_to_base64string(image)
        video_media.append(message)

    # Audio
    for audio_file_name in audio_media_names:
        message = CCU.base_message('audio')
        with open("library/" + libname + '/' + audio_file_name, 'rb') as file:
            audio = file.read()
            message['audio'] = CCU.binary_to_base64string(audio)
        audio_media.append(message)

    # print(video_media[:1])
    # print(audio_media[:1])


def main():
    dirname = ''

    if len(sys.argv) >= 2:
        dirname = str(sys.argv[1])
    else:
        print("Select media to stream...")

        target = []

        for root, dirs, files in os.walk("library", topdown=False):
           number = 1
           for name in dirs:
              print(str(number) + '. ' + name)
              number += 1
           target = dirs
        

        while True:
            sel = input('Which number? ')

            if not sel.isdigit():
                continue

            selection = int(sel)
            
            low = 1
            high = len(target) 

            if selection < low or selection > high:
                continue

            #print('You selected {}'.format(target[selection-1]))
            dirname = target[selection-1]
            break

    audio_media_names = []
    video_media_names = []
    audio_media = []
    video_media = []

    load_media(dirname, audio_media_names, video_media_names, audio_media, video_media)
    stream_media(audio_media, video_media)


if __name__ == "__main__":
    main()

