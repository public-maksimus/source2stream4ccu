import json
import time
import os
import sys
import uuid

import zmq
from ccu import CCU
from datetime import datetime


FREQ = 10


def stream_media(video_media):
    print('Streaming...')

    video_socket = CCU.socket(CCU.queues['VIDEO_MAIN'], zmq.PUB)

    processing_num = 0
    start_time = time.time()

    while processing_num < len(video_media):
        video_msg = video_media[processing_num]
        #video_msg['count'] = processing_num
        video_msg['timestamp'] = .1 * processing_num
        video_msg['datetime'] = str(datetime.now())
        #video_msg['uuid'] = uuid.uuid4()
        video_msg['width'] = 1920
        video_msg['height'] = 1080
        video_msg['depth'] = 3
        video_msg['format'] = 'jpg'
        video_msg['container_name'] = 'parc-stream-publish-video'
        video_msg['trigger_id'] = {}
        CCU.send(video_socket, video_msg)

        end_time = time.time()

        time.sleep(((1/FREQ) - 0.0005) - (end_time- start_time))

        #while((end_time- start_time) < (1/FREQ)):
        end_time = time.time()            

        print('Sent messages {} of {} with a time delta of {} and datetime of {}'.format(processing_num, 
            len(video_media), str(end_time- start_time), video_msg['datetime']))

        processing_num += 1

        start_time = end_time


def load_media(libname, video_media_names, video_media):
    print('Loading {}...'.format(libname))

    for root, dirs, files in os.walk("library/" + libname, topdown=False):
       number = 1
       for name in files:
          if 'jpg' in name:
            video_media_names.append(name)

    video_media_names.sort()


    # Video
    for image_file_name in video_media_names:
        message = CCU.base_message('webcam')
        with open("library/" + libname + '/' + image_file_name, 'rb') as file:
            image = file.read()
            message['image'] = CCU.binary_to_base64string(image)
        video_media.append(message)


def main():
    dirname = ''

    if len(sys.argv) >= 2:
        dirname = str(sys.argv[1])
    else:
        print("Select media to stream...")

        target = []

        for root, dirs, files in os.walk("library", topdown=False):
           number = 1
           for name in dirs:
              print(str(number) + '. ' + name)
              number += 1
           target = dirs
        

        while True:
            sel = input('Which number? ')

            if not sel.isdigit():
                continue

            selection = int(sel)
            
            low = 1
            high = len(target) 

            if selection < low or selection > high:
                continue

            #print('You selected {}'.format(target[selection-1]))
            dirname = target[selection-1]
            break

    video_media_names = []
    video_media = []

    load_media(dirname, video_media_names, video_media)
    stream_media(video_media)


if __name__ == "__main__":
    main()

