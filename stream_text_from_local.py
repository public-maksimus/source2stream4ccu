import json
import time
import os
import sys
import uuid

import zmq
from ccu import CCU
from datetime import datetime
from xml.dom import minidom


class TextData:
    def __init__(self, id, start, end, text, uuid):
        self.id = id 
        self.start = start 
        self.end = end
        self.text = text 
        self.uuid = uuid

    def __str__(self):
        return 'Text data id={}, start={}, end={}, text={} uuid={}'.format(self.id, self.start, self.end, self.text, self.uuid)


def stream_data(data):
    print('Streaming...')

    result_socket = CCU.socket(CCU.queues['RESULT'], zmq.PUB)

    processing_num = 0

    for dat in data:
        result_msg = CCU.base_message('ldc_text_ltf')
        result_msg['datetime'] = str(datetime.now())
        result_msg['num_samples'] = len(data)
        result_msg['count'] = processing_num 
        result_msg['start'] = dat.start
        result_msg['end'] = dat.end
        result_msg['text'] = dat.text
        result_msg['uuid'] = dat.uuid

        CCU.send(result_socket, result_msg)

        print('Sent messages {} of {}'.format(processing_num + 1, len(data)))
        processing_num += 1


def main():

    if len(sys.argv) >= 2:
        input_source = str(sys.argv[1])
    else:
        print("Need to supply path and text name: python3 process_audio_from_local.py {/path/and/txt.ltf.xml}")

    data = []

    # Process XML file
    file = minidom.parse(input_source)
    models = file.getElementsByTagName('SEG')

    for model in models:
        dataObject = TextData(str(model.attributes['id'].value), 
                              int(model.attributes['start_char'].value), 
                              int(model.attributes['end_char'].nodeValue),
                              str(model.childNodes[1].firstChild.nodeValue), 
                              str(uuid.uuid4()))

        print('Created {}'.format(dataObject))

        data.append(dataObject)

    stream_data(data)


if __name__ == "__main__":
    main()

