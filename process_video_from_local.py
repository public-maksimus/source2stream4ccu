import os
import sys
import numpy as np

import datetime
import time
import shutil


from datetime import timedelta
from yaspin import yaspin
from moviepy.editor import VideoFileClip
from datetime import timedelta
from pydub import AudioSegment
from pydub.utils import make_chunks


# Globals
VIDEO_HZ = 10
AUDIO_HZ = 10
target_dir = ''


# Objects and functions passed to YouTube_DL
#

class MyLogger(object):
    '''
        Passed in the youtube_dl options as a pointer to this class
    '''
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    '''
        Passed in the youtube_dl options as a pointer to this function
    '''
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


# Main sections of Code (not OO)
#


def format_timedelta(td):
    '''
        Utility function to format timedelta objects in a cool way (e.g 00:00:20.05) 
        omitting microseconds and retaining milliseconds
    '''
    result = str(td)
    
    try:
        result, ms = result.split(".")
    except ValueError:
        return result + ".00".replace(":", "-")
    
    ms = int(ms)
    ms = round(ms / 1e4)
    
    return f"{result}.{ms:02}".replace(":", "-")


def chop_video(video_filename):
    # load the video clip
    video_clip = VideoFileClip(video_filename)
    
    # make a folder by the name of the video file
    filename, _ = os.path.splitext(video_filename)

    if not os.path.isdir('library'):
        os.mkdir('library/')

    filename = 'library/' + filename
    #filename = 'library/' + filename.rsplit('-',1)[1]
    
    if not os.path.isdir(filename):
        os.mkdir(filename)

    target_dir = filename

    # if the SAVING_FRAMES_PER_SECOND is above video FPS, then set it to FPS (as maximum)
    saving_frames_per_second = min(video_clip.fps, VIDEO_HZ)
    
    # if SAVING_FRAMES_PER_SECOND is set to 0, step is 1/fps, else 1/SAVING_FRAMES_PER_SECOND
    step = 1 / video_clip.fps if saving_frames_per_second == 0 else 1 / saving_frames_per_second
    
    # iterate over each possible frame
    for current_duration in np.arange(0, video_clip.duration, step):
        # format the file name and save it
        frame_duration_formatted = format_timedelta(timedelta(seconds=current_duration)).replace(":", "-")
        frame_filename = os.path.join(filename, f"{frame_duration_formatted}-video.jpg")
        # save the frame with the current duration
        video_clip.save_frame(frame_filename, current_duration)


def chop_audio_alt(audio_filename):
    length = (1000//AUDIO_HZ)
    t1 = 0
    t2 = t1 + length
    chunks = []

    myaudio = AudioSegment.from_wav(audio_filename) 
    duration = myaudio.duration_seconds * 1000

    while t1 < duration:
        chunks.append((t1, myaudio[t1:t2]))
        t1 = t2
        t2 += length

    # make a folder by the name of the video file
    filename, _ = os.path.splitext(audio_filename)
    #filename = 'library/' + filename.rsplit('-',1)[1]
    filename = 'library/' + filename

    #Export all of the individual chunks as wav files
    for (t1, chunk) in chunks:
        frame_duration_formatted = format_timedelta(timedelta(milliseconds=t1)).replace(":", "-")
        chunk_name = os.path.join(filename, f"{frame_duration_formatted}-audio.wav")
        chunk.export(chunk_name, format="wav")

def yes_or_no(question):
    reply = str(input(question+' (y/n): ')).lower().strip()
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return yes_or_no("Uhhhh... please enter ")


def main():

    if len(sys.argv) >= 2:
        input_source = str(sys.argv[1])
    else:
        print("Need to supply path and movie name: python3 process_video_from_local.py {/path/and/movie.mp4.ldcc}")

    # Get name of clip
    clip_name = input_source.rsplit('/', 1)[1].split('.')[0]
    source_filename = clip_name + '.mp4'

    # Strip out LDC header
    if yes_or_no('Strip out LDC header?'):
        linux_function_call = 'dd if=' + input_source + ' of=' + source_filename + ' ibs=1024 skip=1'
        print('Stripping out LDC cruft: {}'.format(linux_function_call))
        os.system(linux_function_call)
        print('Made clean video file: {}'.format(source_filename))
    else:
        source_filename = input_source

    # Remove audio from movie
    videoclip = VideoFileClip(source_filename)
    new_clip = videoclip.without_audio()

    video_filename = clip_name + '-a.mp4'
    new_clip.write_videofile(video_filename)
    print('Made video only file: {}'.format(video_filename))
    
    # Place audio into own file
    audioclip = videoclip.audio
    audio_filename = clip_name + '-a.wav'
    audioclip.write_audiofile(audio_filename)
    print('Made audio only file: {}'.format(audio_filename))

    spinner = yaspin()

    # Chop video into VIDEO Hz
    print('===============---------- Chopping video...')
    #video_filename = '让闺蜜假装掀桌子跟男友吵架，结果居然...-76367531.mp4'
    spinner.start()
    chop_video(video_filename)
    spinner.ok('Video chopped up')
    spinner.stop()

    # Chop video into AUDIO Hz
    print('===============---------- Chopping audio...')
    #audio_filename = '让闺蜜假装掀桌子跟男友吵架，结果居然...-76367531.wav'
    spinner.start()
    chop_audio_alt(audio_filename)
    spinner.ok('Audio chopped up')
    spinner.stop()

    time.sleep(1)

    print('Cleaning up files.')

    if not os.path.isdir('sources'):
        os.mkdir('sources')

    shutil.move(source_filename, 'sources/' + clip_name + '.mp4')
    shutil.move(video_filename, 'sources/' + video_filename)
    shutil.move(audio_filename, 'sources/' + audio_filename)

    print('Processing complete.')


if __name__ == "__main__":
    main()