import json
import time
import base64
import zmq
import sys

from ccu import CCU
from PIL import Image
from numpy import asarray
from moviepy.editor import *
from io import BytesIO

FRAME_LIMIT = 600
FPS = 10
SAVE_IMAGE_FILES = False

savename = ''

if len(sys.argv) >= 2:
    savename = str(sys.argv[1])
else:
    savename = input('Name to save movie? : ')

if len(savename) < 5:
    savename = 'movie.mp4'

print('Starting the image frame subscriber.  Will block waiting for message.')
socket = CCU.socket(CCU.queues['VIDEO_MAIN'], zmq.SUB)

clips = []
number = 0

while number < FRAME_LIMIT:
    message = CCU.recv_block(socket)
    if (message['type'] == 'webcam'):
        #image = CCU.base64string_to_binary(message['image']) # Note: not good image this method
        im = Image.open(BytesIO(base64.b64decode(message['image'])))
        rgb_im = im.convert("RGB")

        if SAVE_IMAGE_FILES:
            filename = "images/image-{}.jpg".format(str(number))
            rgb_im.save(filename)

        print(f'Got frame {number + 1}.  Adding to queue.')
        clips.append(asarray(rgb_im))
    else:
        print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

    number += 1

print('Creating the video')
clip = ImageSequenceClip(clips, fps=FPS)
clip.write_videofile(savename)
