import json
import time
import base64
import zmq
import sys
import wave
import _thread

from ccu import CCU
from PIL import Image
from numpy import asarray
from moviepy.editor import *
from io import BytesIO

FRAME_LIMIT = 300
FPS = 10
SAVE_IMAGE_FILES = False


def gather_video(clips, number):
    '''
        Threaded function
    '''

    socket = CCU.socket(CCU.queues['VIDEO_MAIN'], zmq.SUB)

    while number['value'] < FRAME_LIMIT:
        message = CCU.recv_block(socket)
        if (message['type'] == 'webcam'):
            im = Image.open(BytesIO(base64.b64decode(message['image'])))
            rgb_im = im.convert("RGB")
            clips.append(asarray(rgb_im))

            print('Got video frame {}.  Adding to queue.'.format(number['value'] + 1))
        else:
            print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

        number['value'] = number['value'] + 1


def gather_audio(data, counter):
    '''
        Threaded function
    '''

    print('Started audio thread...')

    mysocket = CCU.socket(CCU.queues['AUDIO_ENV'], zmq.SUB)

    while counter['value'] < FRAME_LIMIT:
        print('check')

        message = CCU.recv_block(mysocket)

        if len(message) > 0:
            print('Got message')

        if (message['type'] == 'audio'):
            w = wave.open(BytesIO(base64.b64decode(message['audio'])))
            data.append([w.getparams(), w.readframes(w.getnframes())])
            w.close()

            print('Got audio frame {}.  Adding to queue.'.format(counter['value'] + 1))
        else:
            print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

        counter['value'] = counter['value'] + 1

def combined_gather(clips, number, data, counter):
    video_socket = CCU.socket(CCU.queues['VIDEO_MAIN'], zmq.SUB)
    audio_socket = CCU.socket(CCU.queues['AUDIO_ENV'], zmq.SUB)

    while (number['value'] < FRAME_LIMIT) and (counter['value'] < FRAME_LIMIT):

        # Video Section
        video_message = CCU.recv_block(video_socket)

        if (video_message['type'] == 'webcam'):
            im = Image.open(BytesIO(base64.b64decode(video_message['image'])))
            rgb_im = im.convert("RGB")
            clips.append(asarray(rgb_im))

            print('Got video frame {}.  Adding to queue.'.format(number['value'] + 1))
        else:
            print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

        number['value'] = number['value'] + 1    

        # Audio Section
        audio_message = CCU.recv_block(audio_socket)

        if (audio_message['type'] == 'audio'):
            w = wave.open(BytesIO(base64.b64decode(audio_message['audio'])))
            data.append([w.getparams(), w.readframes(w.getnframes())])
            w.close()

            print('Got audio frame {}.  Adding to queue.'.format(counter['value'] + 1))
        else:
            print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

        counter['value'] = counter['value'] + 1


def main():
    savename = ''

    if len(sys.argv) >= 2:
        savename = str(sys.argv[1])
    else:
        savename = input('Name to save movie with audio? : ')

    if len(savename) < 5:
        savename = 'movie.mp4'

    print('Starting the image and audio frame subscriber.  Will block waiting for message.')

    clips = []
    number = {'value' : 0}

    data = []
    counter = {'value' : 0}

    combined_gather(clips, number, data, counter)

    # try:
    #    _thread.start_new_thread( gather_audio, (data, counter, ) )
    #    _thread.start_new_thread( gather_video, (clips, number, ) )
    # except:
    #    print ("Error: unable to start thread")

    last_clip_length = 0
    last_data_length = 0

    while 1:
        if len(clips) != last_clip_length or len(data) != last_data_length:
            print('Length of video queue is {} and audio queue is {}'.format(len(clips), len(data)))
            last_clip_length = len(clips)
            last_data_length = len(data)

        if len(clips) == FRAME_LIMIT and len(data) == FRAME_LIMIT:
            break

    print('Creating the video')
    clip = ImageSequenceClip(clips, fps=FPS)
    
    # Note: This next session writes the audio out to a file to combine with the video
    #       because moviepy library is a piece of crap and will not take a stream or even 
    #       a numpy array like their docs say
    #       

    print('Creating the audio')
    output = wave.open('tmp.wav', 'wb')
    output.setparams(data[0][0])
    for i in range(len(data)):
        output.writeframes(data[i][1])
    output.close()
    audioclip = AudioFileClip('tmp.wav')

    print('Creating single output file {}'.format(savename))
    clip.audio = audioclip
    clip.write_videofile(savename)

    os.remove("tmp.wav")
    

if __name__ == "__main__":
    main()