# source2stream4ccu

Takes a video from YouTube, Bili Bili, and other services (you have to test them to see if they work) as well as local video files and chops the video stream into an X Hz set of JPEG images, where X = 10 by default.  The audio stream is also chopped into 1/Y second segments, where Y = 10 by default. Both elements are streamed over ZeroMQ and require a non-public message infrastructure piece from SRI (CCU Project).

There are scripts that will also process just LDC audio and ltf text files for streaming. ltf text files do not have intermediate forms and are directly streamed.

There are also scripts to stitch streaming audio, video, and audio+video streams back together for round trip analysis.

## Installation

Edit the install file to include where the location of ccu-0.9-py3-none-any.whl (or newer version) from SRI.

```
$: ./install.sh
```

This will create a virtual environment `VENV` and load the appropriate dependencies.


## Beginning a session

Always run this code in a virtual environment, which can ve activated by from within the project directory after installation
```
$: source VENV/bin/activate
```
You know when you are in a virtual environment by the virtual environment name existing in parentheses next to your command prompt.
```
(VENV) $:
```

## Usage

-----

### Application: Video with audio retrieval and segmenter

Run:
```
(VENV) $: python retrieve_and_chop.py {OPTIONAL: Specify URL}
```

This will either load and process from the commandline specified URL or will prompt the user to enter a URL if not provided. The example URL will be loaded if no URL is specified and _Enter_ input.


Processed video and audio segments will be placed under the `library/` directory and the video sources under `sources/`


### Application: processing LDC provided data


#### Video with Audio

Run:
```
(VENV) $: python process_video_from_local.py {path and filename of LDC video file}
```

Example:
```
(VENV) $: python3 process_from_local.py ~/parc-maksimus/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/data/video/M01000G2T.mp4.ldcc
```
This will remove the LDC header from the video, create separate video and audio files, and then provide the chopped-up versions for streaming like from online sources. These will be ready for publishing video and audio streams.

A prompt will ask if you want to strip out the LDC header. Removing a header when not present will cause issues as the first 1024 bytes will be chopped from the file.

#### Audio

Run:
```
(VENV) $: python process_audio_from_local.py {path and filename of LDC audio file}
```

Example:
```
(VENV) $: python3 process_audio_from_local.py ~/parc-maksimus/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/data/audio/M01000537.flac.ldcc
```

This will remove the LDC header from the audio, and then provide the chopped-up version for streaming like from online sources. This will be ready for publishing an audio stream.

A prompt will ask if you want to strip out the LDC header. Removing a header when not present will cause issues as the first 1024 bytes will be chopped from the file.

#### Text

Run:
```
(VENV) $: python stream_text_from_local.py {path and filename of LDC lmf text file}
```

Example:
```
(VENV) $: python3 stteam_text_from_local.py ~/parc-maksimus/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/LDC2022E11_CCU_TA1_Mandarin_Chinese_Development_Source_Data_R1/data/text/ltf/M01000H0B.ltf.xml
```

This will directly stream the text.

Processed video and audio segments will be placed under the `library/` directory and the sources under `sources/`

### Application: Publishing Streamers

Streamers try to stream in 10Hz by default to match a live stream, but you may change the `FREQ` value in the code (just under the library loads) to a large number to increase the speed.

Run for video and audio streaming:

```
(VENV) $: python stream_publish.py {OPTIONAL library directory name}
```
If a directory is not provided on the commandline then a selection list will be provided. This will stream the video and audio on separate channels at 10 Hz.


Run for video only streaming:

```
(VENV) $: python stream_publish_video_only.py {OPTIONAL library directory name}
```
Run for audio only streaming:

```
(VENV) $: python stream_publish_audio_only.py {OPTIONAL library directory name}
```

### Application: Subscribing Stitchers

Run for receiving and stitching together published video and audio streaming:
```
(VENV) $: python stream_stitch.py {OPTIONAL output filename}
```
If a filename is not provided on the commandline then a prompt for one will be provided. This will collect the stream and assemble a single video + audio file. Specify the format that you want the video in (e.g., name.mp4). If it cannot create that format due to a lack of codec in your system, it will complain.

Run for receiving and stitching together published video only streaming:
```
(VENV) $: python stream_stitch_video_only.py {OPTIONAL output filename}
```

Specify the format that you want the video in (e.g., name.mp4). If it cannot create that format due to a lack of codec, it will complain.

Run for receiving and stitching together published audio only streaming:
```
(VENV) $: python stream_stitch_audio_only.py {OPTIONAL output filename}
```

This will produce a wav file.

-----

## Support
Contact Michael Youngblood <Michael.Youngblood@parc.com>

## Roadmap
Always improving...

## Contributing
If you have direct access to this repository, please feel free to make modifications and add yourself as an author below. If you break it, you must fix it. If you don't have direct access, please make the changes and submit a merge request through the repository system. If accepted, you will be added as an author. If you do not hear or see a status change within 2 weeks, please contact support above.

## Authors and acknowledgment

G. Michael Youngblood, PARC

{Add your name here if you contributed to the code base.}


## License
“Commons Clause” License Condition v1.0

The Software is provided to you by the Licensor under the License, as defined below, subject to the following condition.

Without limiting other conditions in the License, the grant of rights under the License will not include, and the License does not grant to you, the right to Sell the Software.

For purposes of the foregoing, “Sell” means practicing any or all of the rights granted to you under the License to provide to third parties, for a fee or other consideration (including without limitation fees for hosting or consulting/ support services related to the Software), a product or service whose value derives, entirely or substantially, from the functionality of the Software. Any license notice or attribution required by the License must also include this Commons Clause License Condition notice.

Software: source2stream4ccu

License: Apache 2.0 See license.txt

Licensor: Xerox PARC 

## Disclaimers
This is research code. It will be messy and buggy. If you feel that you need to complain, stop, just fix it or live with it.


©2022 Xerox PARC. All Rights Reserved.

