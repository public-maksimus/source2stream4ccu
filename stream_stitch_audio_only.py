import json
import time
import base64
import zmq
import wave
import sys

from ccu import CCU
from io import BytesIO

FRAME_LIMIT = 600
FPS = 10

savename = ''

if len(sys.argv) >= 2:
    savename = str(sys.argv[1])
else:
    savename = input('Name to save audio? : ')

if len(savename) < 5:
    savename = 'audio.wav'

print('Starting the audio frame subscriber.  Will block waiting for message.')
socket = CCU.socket(CCU.queues['AUDIO_ENV'], zmq.SUB)

data = []
number = 0

while number < FRAME_LIMIT:
    message = CCU.recv_block(socket)
    if (message['type'] == 'audio'):
        #image = CCU.base64string_to_binary(message['image']) # Note: not good image this method
        w = wave.open(BytesIO(base64.b64decode(message['audio'])))
        data.append([w.getparams(), w.readframes(w.getnframes())])
        w.close()

        print(f'Got frame {number + 1}.  Adding to queue.')
    else:
        print('Got a message of an unexpected type {message["type"]}.  Ignoring.')

    number += 1


output = wave.open(savename, 'wb')
output.setparams(data[0][0])

for i in range(len(data)):
    output.writeframes(data[i][1])

output.close()

