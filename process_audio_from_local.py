import os
import sys
import numpy as np
import youtube_dl
import datetime
import time
import shutil
import soundfile

from datetime import timedelta
from yaspin import yaspin
#from moviepy.editor import VideoFileClip
from datetime import timedelta
from pydub import AudioSegment
from pydub.utils import make_chunks


# Globals
AUDIO_HZ = 10
target_dir = ''


def format_timedelta(td):
    '''
        Utility function to format timedelta objects in a cool way (e.g 00:00:20.05) 
        omitting microseconds and retaining milliseconds
    '''
    result = str(td)
    
    try:
        result, ms = result.split(".")
    except ValueError:
        return result + ".00".replace(":", "-")
    
    ms = int(ms)
    ms = round(ms / 1e4)
    
    return f"{result}.{ms:02}".replace(":", "-")


def chop_audio_alt(audio_filename):
    length = (1000//AUDIO_HZ)
    t1 = 0
    t2 = t1 + length
    chunks = []

    myaudio = AudioSegment.from_wav(audio_filename) 
    duration = myaudio.duration_seconds * 1000

    while t1 < duration:
        chunks.append((t1, myaudio[t1:t2]))
        t1 = t2
        t2 += length

    # # make a folder by the name of the video file
    # filename, _ = os.path.splitext(audio_filename)
    # #filename = 'library/' + filename.rsplit('-',1)[1]
    # filename = 'library/' + filename

    # make a folder by the name of the video file
    filename, _ = os.path.splitext(audio_filename)

    if not os.path.isdir('library'):
        os.mkdir('library/')

    filename = 'library/' + filename
    #filename = 'library/' + filename.rsplit('-',1)[1]
    
    if not os.path.isdir(filename):
        os.mkdir(filename)

    #Export all of the individual chunks as wav files
    for (t1, chunk) in chunks:
        frame_duration_formatted = format_timedelta(timedelta(milliseconds=t1)).replace(":", "-")
        chunk_name = os.path.join(filename, f"{frame_duration_formatted}-audio.wav")
        chunk.export(chunk_name, format="wav")

def yes_or_no(question):
    reply = str(input(question+' (y/n): ')).lower().strip()
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return yes_or_no("Uhhhh... please enter ")


def main():

    if len(sys.argv) >= 2:
        input_source = str(sys.argv[1])
    else:
        print("Need to supply path and audio name: python3 process_audio_from_local.py {/path/and/audio.flacc.ldcc}")

    # Get name of clip
    clip_name = input_source.rsplit('/', 1)[1].split('.')[0]
    source_filename = clip_name + '.flac'

    # Strip out LDC header
    if yes_or_no('Strip out LDC header?'):
        linux_function_call = 'dd if=' + input_source + ' of=' + source_filename + ' ibs=1024 skip=1'
        print('Stripping out LDC cruft: {}'.format(linux_function_call))
        os.system(linux_function_call)
        print('Made clean audio file: {}'.format(source_filename))
    else:
        source_filename = input_source

    # Convert flac to wav
    wav_file = r'{}'.format(source_filename)
    audio, sr = soundfile.read(wav_file)
    audio_filename = '{}.wav'.format(clip_name)
    soundfile.write(audio_filename, audio, sr, 'PCM_16')

    # Chop video into AUDIO Hz
    print('===============---------- Chopping audio...')
    spinner = yaspin()
    spinner.start()
    chop_audio_alt(audio_filename)
    spinner.ok('Audio chopped up')
    spinner.stop()

    time.sleep(1)

    print('Cleaning up files.')

    if not os.path.isdir('sources'):
        os.mkdir('sources')

    shutil.move(source_filename, 'sources/' + source_filename)
    shutil.move(audio_filename, 'sources/' + audio_filename)
    print('Processing complete.')


if __name__ == "__main__":
    main()