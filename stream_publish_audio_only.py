import json
import time
import os
import sys
import uuid

import zmq
from ccu import CCU
from datetime import datetime


FREQ = 10


def stream_media(audio_media):
    print('Streaming...')

    audio_socket = CCU.socket(CCU.queues['AUDIO_ENV'], zmq.PUB)

    processing_num = 0
    start_time = time.time()
    mark_start_time = datetime.now()
    audio_id = str(uuid.uuid4())
    sample = 0

    while processing_num < len(audio_media):

        audio_msg = audio_media[processing_num]
        #audio_msg['count'] = processing_num
        audio_msg['timestamp'] = .1 * processing_num
        audio_msg['datetime'] = str(datetime.now())
        #audio_msg['uuid'] = uuid.uuid4()
        audio_msg['sample_rate'] = 16000
        audio_msg['bit_depth'] = 16
        audio_msg['num_samples'] = len(audio_media)
        audio_msg['channels'] = 1
        audio_msg['start_sample'] = 0
        audio_msg['sample'] = sample
        sample += 1
        mark_time = datetime.now()
        delta = mark_time - mark_start_time
        audio_msg['start_seconds'] = float(delta.total_seconds())
        audio_msg['audio_id'] = audio_id
        audio_msg['speaker'] = 'FLE'
        audio_msg['container_name'] = 'parc-stream-publish-audio'
        audio_msg['trigger_id'] = {}
        #audio_msg['start_time'] = mark_start_time
        CCU.send(audio_socket, audio_msg)

        end_time = time.time()

        time.sleep(((1/FREQ) - 0.0005) - (end_time- start_time))

        #while((end_time- start_time) < (1/FREQ)):
        end_time = time.time()            

        print('Sent messages {} of {} with a time delta of {} and datetime of {}'.format(processing_num, 
            len(audio_media), str(end_time- start_time), audio_msg['datetime']))

        processing_num += 1

        start_time = end_time


def load_media(libname, audio_media_names, audio_media):
    print('Loading {}...'.format(libname))

    for root, dirs, files in os.walk("library/" + libname, topdown=False):
       number = 1
       for name in files:
          if 'wav' in name:
            audio_media_names.append(name)

    audio_media_names.sort()

    # Audio
    for audio_file_name in audio_media_names:
        message = CCU.base_message('audio')
        with open("library/" + libname + '/' + audio_file_name, 'rb') as file:
            audio = file.read()
            message['audio'] = CCU.binary_to_base64string(audio)
        audio_media.append(message)


def main():
    dirname = ''

    if len(sys.argv) >= 2:
        dirname = str(sys.argv[1])
    else:
        print("Select media to stream...")

        target = []

        for root, dirs, files in os.walk("library", topdown=False):
           number = 1
           for name in dirs:
              print(str(number) + '. ' + name)
              number += 1
           target = dirs
        

        while True:
            sel = input('Which number? ')

            if not sel.isdigit():
                continue

            selection = int(sel)
            
            low = 1
            high = len(target) 

            if selection < low or selection > high:
                continue

            #print('You selected {}'.format(target[selection-1]))
            dirname = target[selection-1]
            break

    audio_media_names = []
    audio_media = []

    load_media(dirname, audio_media_names, audio_media)
    stream_media(audio_media)


if __name__ == "__main__":
    main()

