#!/bin/bash

activate() {
		source ./VENV/bin/activate
		pip3 install wheel
		pip3 install -r requirements.txt
}

rm -rf VENV
python3 -m venv VENV
activate

pip install ../sri_ccu_system/python/dist/ccu-1.1-py3-none-any.whl
