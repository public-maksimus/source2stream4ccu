import os
import sys
import numpy as np
import youtube_dl
import datetime
import time
import shutil
import pyaudioconvert as pac

from PIL import Image
from datetime import timedelta
from yaspin import yaspin
from moviepy.editor import VideoFileClip
from datetime import timedelta
from pydub import AudioSegment
from pydub.utils import make_chunks


# Globals
VIDEO_HZ = 10
AUDIO_HZ = 10
target_dir = ''


# Objects and functions passed to YouTube_DL
#

class MyLogger(object):
    '''
        Passed in the youtube_dl options as a pointer to this class
    '''
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    '''
        Passed in the youtube_dl options as a pointer to this function
    '''
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


# Main sections of Code (not OO)
#


def format_timedelta(td):
    '''
        Utility function to format timedelta objects in a cool way (e.g 00:00:20.05) 
        omitting microseconds and retaining milliseconds
    '''
    result = str(td)
    
    try:
        result, ms = result.split(".")
    except ValueError:
        return result + ".00".replace(":", "-")
    
    ms = int(ms)
    ms = round(ms / 1e4)
    
    return f"{result}.{ms:02}".replace(":", "-")


def chop_video(video_filename):
    # load the video clip
    video_clip = VideoFileClip(video_filename)
    
    # make a folder by the name of the video file
    filename, _ = os.path.splitext(video_filename)

    if not os.path.isdir('library'):
        os.mkdir('library/')

    filename = 'library/' + filename
    #filename = 'library/' + filename.rsplit('-',1)[1]
    
    if not os.path.isdir(filename):
        os.mkdir(filename)

    target_dir = filename

    # if the SAVING_FRAMES_PER_SECOND is above video FPS, then set it to FPS (as maximum)
    saving_frames_per_second = min(video_clip.fps, VIDEO_HZ)
    
    # if SAVING_FRAMES_PER_SECOND is set to 0, step is 1/fps, else 1/SAVING_FRAMES_PER_SECOND
    step = 1 / video_clip.fps if saving_frames_per_second == 0 else 1 / saving_frames_per_second
    
    # iterate over each possible frame
    for current_duration in np.arange(0, video_clip.duration, step):
        # format the file name and save it
        frame_duration_formatted = format_timedelta(timedelta(seconds=current_duration)).replace(":", "-")
        frame_filename = os.path.join(filename, f"{frame_duration_formatted}-video.jpg")

        # save the frame with the current duration, making sure it is 1080x1920
        numpy_image = video_clip.get_frame(current_duration )
        PIL_image = Image.fromarray(np.uint8(numpy_image)).convert('RGB')
        PIL_image = PIL_image.resize((1920, 1080))
        PIL_image.save(frame_filename)

        #video_clip.save_frame(frame_filename, PIL_image)


def chop_audio_alt(in_audio_filename):
    audio_filename = 'downsampled.wav'
    pac.convert_wav_to_16bit_mono(in_audio_filename, audio_filename)

    length = (1000//AUDIO_HZ)
    t1 = 0
    t2 = t1 + length
    chunks = []

    myaudio = AudioSegment.from_wav(audio_filename) 
    duration = myaudio.duration_seconds * 1000

    while t1 < duration:
        chunks.append((t1, myaudio[t1:t2]))
        t1 = t2
        t2 += length

    # make a folder by the name of the video file
    filename, _ = os.path.splitext(in_audio_filename)
    #filename = 'library/' + filename.rsplit('-',1)[1]
    filename = 'library/' + filename


    #Export all of the individual chunks as wav files
    for (t1, chunk) in chunks:
        frame_duration_formatted = format_timedelta(timedelta(milliseconds=t1)).replace(":", "-")
        chunk_name = os.path.join(filename, f"{frame_duration_formatted}-audio.wav")
        chunk.export(chunk_name, format="wav")


def main():

    # video_filename = 'EPfvLKPW3ro.mp4'
    # audio_filename = 'EPfvLKPW3ro.wav'

    if len(sys.argv) >= 2:
        yt_url = str(sys.argv[1])
    else:
        # Get YouTube URL
        yt_url = input('YouTube URL (e.g., https://www.youtube.com/watch?v=EPfvLKPW3ro): ')

        if len(yt_url) < 1:
            yt_url = 'https://www.youtube.com/watch?v=EPfvLKPW3ro'

    if len(yt_url) < 25:
        print('Hmmm, doesn\'t seem like a YouTube URL???')
        quit()

    print('===============---------- Getting Video...')

    # Video Options
    ydl_opts_video = {
        'format': 'bestvideo[height=1080]/best',
        'postprocessors': [{
            'key': 'FFmpegVideoConvertor',
            'preferedformat': 'mp4',
            #'preferredquality': '192',
        }],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        'outtmpl' : '%(id)s.%(ext)s',
    }

    # Download Video
    spinner = yaspin()
    spinner.start()
    video_filename = ''
    #video_title = ''
    with youtube_dl.YoutubeDL(ydl_opts_video) as ydl:
        info = ydl.extract_info(yt_url)
        #video_filename = "{}-{}.mp4".format(info['title'], info['id'])
        video_filename = "{}.mp4".format(info['id'])
        #video_title = info['title']
        #video_filename = ydl.prepare_filename(info)

    spinner.ok('Video Downloaded: {}'.format(video_filename))
    spinner.stop()

    print('===============---------- Getting Audio...')

    # Audio Options
    ydl_opts_audio = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'wav',
            'preferredquality': '256',
        }],
        'postprocessor_args': [
        '-ar', '48000',
        '-ac', '1'
        ],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        'outtmpl' : '%(id)s.%(ext)s',
    }

    # Download Audio
    spinner.start()
    audio_filename = ''
    #audio_title = ''
    with youtube_dl.YoutubeDL(ydl_opts_audio) as ydl:
        info = ydl.extract_info(yt_url)
        #audio_filename = "{}-{}.wav".format(info['title'], info['id'])
        audio_filename = "{}.wav".format(info['id'])
        #audio_title = info['title']
        #audio_filename = ydl.prepare_filename(info)

    spinner.ok('Audio Downloaded: {}'.format(audio_filename))
    spinner.stop()

    #target_dir = 'library/让闺蜜假装掀桌子跟男友吵架，结果居然...-76367531'

    # Chop video into VIDEO Hz
    print('===============---------- Chopping video...')
    #video_filename = '让闺蜜假装掀桌子跟男友吵架，结果居然...-76367531.mp4'
    spinner.start()
    chop_video(video_filename)
    spinner.ok('Video chopped up')
    spinner.stop()

    # Chop video into AUDIO Hz
    print('===============---------- Chopping audio...')
    #audio_filename = '让闺蜜假装掀桌子跟男友吵架，结果居然...-76367531.wav'
    spinner.start()
    chop_audio_alt(audio_filename)
    spinner.ok('Audio chopped up')
    spinner.stop()

    time.sleep(1)

    if not os.path.isdir('sources'):
        os.mkdir('sources')

    shutil.move(video_filename, 'sources/' + video_filename)
    shutil.move(audio_filename, 'sources/' + audio_filename)


if __name__ == "__main__":
    main()